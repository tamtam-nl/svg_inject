<?php

namespace Drupal\svg_inject\Twig;

use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;

/**
 * Class SvgInjectExtension
 * Print related nodes
 * @package Drupal\svg_inject\Twig
 */
class SvgInjectExtension extends \Twig_Extension
{
    /**
     * @var \Drupal\Core\Theme\ThemeManagerInterface
     */
    protected $themeManager;
    /**
     * @var \Drupal\Core\Extension\ThemeHandlerInterface
     */
    protected $themeHandler;

    public function __construct(
        ThemeManagerInterface $themeManager,
        ThemeHandlerInterface $themeHandler
    ) {
        $this->themeManager = $themeManager;
        $this->themeHandler = $themeHandler;
    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'svg_inject';
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('svg_inject', [$this, 'svgInject'], [
                'is_safe' => ['html'],
            ]),
        ];
    }

    /**
     * Inject theme svg if exists
     *
     * @param $path
     * @param string $theme
     * @return bool|string
     */
    public function svgInject($path, $theme = '')
    {
        $path = (empty($theme) ? $this->themeManager->getActiveTheme()->getPath() : $this->themeHandler->getTheme($theme)->getPath()) . '/' . $path . '.svg';

        if (file_exists($path)) {
            return file_get_contents($path);
        }
    }
}
